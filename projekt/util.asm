rand: 		mov	a, random
		jnz	randi
		cpl	a
		mov	random, a
randi:		anl	a, #10111000b
		mov	C, P
		mov	a, random
		rlc	a
		mov	random, a
		ret

delay: 		mov a, param
delayi:		djnz a, delayi
		ret

tick: 		inc centis
		mov a, centis
		cjne a, #100, ticki
		call inc_sec
ticki:		ret

inc_sec:	inc seconds
		mov centis, #0
		ret