display: 	mov dptr, #display_numbers
		mov a, centis
		mov b, #10
		div ab
		mov r0, a
		movc a,@a+dptr
		mov r3, a
		mov a, r0
		xch a,b
		movc a, @a+dptr
		mov r2, a
		;----------------
		mov a, seconds
		mov b, #10
		div ab
		mov r0, a
		movc a,@a+dptr
		mov r5, a
		mov a, r0
		xch a,b
		movc a, @a+dptr
		mov r4, a
		call show
		ret

show:		mov time_out2, R2
		clr time_out1.0
		setb time_out1.0
		
		mov time_out2, R3
		clr time_out1.1
		setb time_out1.1
		
		mov time_out2, R4
		clr time_out2.0 ; set point after second digit
		clr time_out1.2
		setb time_out1.2
		
		mov time_out2, R5
		clr time_out1.3
		setb time_out1.3
		
		ret

org 300h
display_numbers:
db 00000011b
db 10011111b, 00100101b, 00001101b
db 10011001b, 01001001b, 01000001b
db 00011111b, 00000001b, 00001001b