; F5

include config.asm

org 100h
main: 		jb user_in.7, main
		jmp start

start: 		mov led_out, #0xff
		mov leds, #0
		mov time_out2, #0
		mov time_out1, #00001111b
		jmp add_led

add_led:	mov param, #0xcc
		call delay
		call rand
		mov a, leds
		orl a, random
		mov leds, a
		; invert leds for output
		mov a, #0xff
		clr c
		subb a, leds
		mov led_out, a
		mov a, leds
		; add more leds if not all on
		cjne a, #0xff, add_led
		jmp wait

wait:		mov centis, #0
		mov seconds, #0
waiti:		call tick
		jnb user_in.6, finish
		jmp waiti

finish: 	mov led_out, #0xff
finishi:	call display
		mov param, #0x22
		call delay
		jb user_in.6, main
		jmp finishi

include util.asm
include output.asm

end